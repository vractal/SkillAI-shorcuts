from time import sleep
from mycroft import MycroftSkill, intent_file_handler
import os
# import dbus

# import pyautogui


class Shorcuts(MycroftSkill):
    def __init__(self):
        MycroftSkill.__init__(self)

    @intent_file_handler('shorcuts.intent')
    def handle_shorcuts(self, message):
        thing = message.data.get('thing')

        if (thing == 'firefox'):
            print('firefox')
            os.system('firefox &')
        elif (thing == 'stremio'):
            os.system('stremio &')

        elif (thing == 'joplin notes' or thing == 'joplin' or thing == 'notes'):
            os.system('~/Applications/Joplin-2.* &')
        # else:
            # bus = dbus.SessionBus()
            # remote_object = bus.get_object("org.kde.krunner", "/App")
            # remote_object.query(
            #     'recent' + ' ', dbus_interface="org.kde.krunner.App")

        # This should navigate .desktop paths and search there for name -> launch script relations
        self.speak_dialog('shorcuts')

    @intent_file_handler('shorcuts.desktop.intent')
    def handle_shorcut_desktop(self, message):
        number = message.data.get('number')
        if (number == 'one' or number == '1'):
            number = '1'
        elif (number == 'two' or number == '2' or number == 'true'):
            number = '2'
        elif (number == 'three' or number == 'tree' or number == '3'):
            number = '3'
        elif (number == 'four' or number == 'for' or number == '4'):
            number = '4'
        else:
            try:
                os.system('xdotool set_desktop $(xdotool get_desktop_for_window $(xdotool search -limit 1 -onlyvisible --name --classname  "' + number + '")) && xdotool windowfocus $(xdotool search -limit 1 -onlyvisible --name --classname "' + number +
                          '") &&  xdotool windowraise $(xdotool search -limit 1 -onlyvisible "' + number + '")')
            except:
                print('error changing desktop')
            return

        os.system('xdotool key --clearmodifiers alt+' + number)

    @ intent_file_handler('shorcuts.close.intent')
    def handle_shorcuts_close(self, message):
        window = message.data.get('window')
        if (window == "window"):
            os.system('xdotool windowkill $(xdotool getactivewindow)')
        elif (isinstance(window, str)):
            os.system(
                'xdotool windowclose $(xdotool search --limit 1 --onlyvisible ' + window + ')')

        self.speak_dialog('shorcuts')

    @ intent_file_handler('shorcuts.track.intent')
    def handle_shorcuts_track(self, message):
        action = message.data.get('action')
        os.system('xdotool key --clearmodifiers ctrl+alt+s')

        self.speak_dialog('shorcuts')

    @ intent_file_handler('shorcuts.media.intent')
    def handle_shorcuts_media(self, message):
        os.system('xdotool key XF86AudioPlay')

        self.speak_dialog('shorcuts')

    @ intent_file_handler('shorcuts.code.intent')
    def handle_shorcuts_code(self, message):
        os.system('xdotool key ctrl+p')
        sleep(1000)
        os.system('xdotool type chat' + message.data.get('file'))

        self.speak_dialog('shorcuts')


def create_skill():
    return Shorcuts()
